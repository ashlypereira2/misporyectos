package com.example.laboratorio2ashlypereira

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class Fragmento1 : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null







    //Codigo para incrementar numeros de uno o mas en el fragmento 1
    var conteo = 0

    //Configurar el objeto contadorlistener

    var contadorListener:ContadorListener? = null

    //Adicionar contador
    fun addContadorListenerFrag1(c:ContadorListener){
        this.contadorListener = c
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        val txtView1 = view.findViewById<TextView>(R.id.txt1)
        txtView1.setText(contadorListener!!.getValorActual().toString())
        val botonContador = view.findViewById<Button>(R.id.incrementar)
        botonContador.setOnClickListener{
            contadorListener!!.incrementar()
            txtView1.setText((contadorListener!!.getValorActual()).toString())
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fragmento1, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragmento1().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}