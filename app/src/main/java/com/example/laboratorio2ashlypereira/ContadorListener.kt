package com.example.laboratorio2ashlypereira

interface ContadorListener {



    //Es para conectar entre varios fragmentos
    fun incrementar()
    fun getValorActual():Int
    fun resetear()
    fun reducir()
}