package com.example.laboratorio2ashlypereira

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import com.example.laboratorio2ashlypereira.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(), ContadorListener {
    //Variables----------------------------
    var fragment1: Fragmento1? = null
    var fragment2: Fragmento2? = null
    var fragment3: Fragmento3? = null
    //----------------------------
    var button1: Button? = null
    var button2: Button? = null
    var button3: Button? = null

    //Implementar la interfaz
    var contadores = 0
    override fun incrementar() {
       contadores++
    }
     override fun getValorActual(): Int {
         return contadores
     }
    override fun resetear(){
        contadores = 0
    }
    override fun reducir(){
        if (contadores>0)contadores--
        else contadores = 0
    }



    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //agregamos el toobar en el main
        binding  = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val toolbar = binding.PrincipalToolbar.Toolbar
        //metodo set
        setSupportActionBar(toolbar)
//Funciones
        fragment1 = Fragmento1()

        //LLamado al contador listener al fragmento 1
        fragment1?.addContadorListenerFrag1(this)

        fragment2 = Fragmento2()

        //LLamado al contador listener al fragmento 2
        fragment2?.addListenerFrag2(this)

        fragment3 = Fragmento3()

        button1 = findViewById(R.id.boton1)
        button2 = findViewById(R.id.boton2)
        button3 = findViewById(R.id.button3)

//Fragmento 1
        button1?.setOnClickListener(  {
            Toast.makeText(this, "Pasando a Fragmento 1",Toast.LENGTH_SHORT).show()
            val transaction = getSupportFragmentManager().beginTransaction()
        transaction.replace(R.id.FragmentoMedio,fragment1!!)
            transaction.commit()
        })
        //Fragmento 2
        button2?.setOnClickListener(  {
            Toast.makeText(this, "Pasando a Fragmento 2",Toast.LENGTH_SHORT).show()
            val transaction = getSupportFragmentManager().beginTransaction()
            transaction.replace(R.id.FragmentoMedio,fragment2!!)
            transaction.commit()
        })
        //Fragmento 3
        button3?.setOnClickListener(  {
            Toast.makeText(this, "Pasando a Fragmento 3",Toast.LENGTH_SHORT).show()
            val transaction = getSupportFragmentManager().beginTransaction()
            transaction.replace(R.id.FragmentoMedio,fragment3!!)
            transaction.commit()
        })
    }

    //metodo para añadir el actionBar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.actionbar, menu)
        return true
    }

    //metodo para que aga accion los botones de guardar y ajustes
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.Guardar -> {
                Toast.makeText(this, "Guardar", Toast.LENGTH_SHORT).show()
                true
            }

            R.id.Ajustes -> {
                Toast.makeText(this, "Ajustes", Toast.LENGTH_SHORT).show()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }

    }

}