package com.example.laboratorio2ashlypereira

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class Fragmento2 : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null


    //Configurar el objeto contadorlistener

    var contadorListenerFrag2:ContadorListener? = null



    //Adicionar contador listener

    fun addListenerFrag2(c:ContadorListener){
        this.contadorListenerFrag2 = c
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?){
        val txt2 = view.findViewById<TextView>(R.id.txt2)
        val btn = view.findViewById<Button>(R.id.Reducir)
        btn.setOnClickListener{
            contadorListenerFrag2?.reducir();
            txt2.setText(contadorListenerFrag2!!.getValorActual().toString())
        }
    }

//Mostrar Fragmento
    override fun onResume() {
    val txt2 = view?.findViewById<TextView>(R.id.txt2)
        txt2!!.setText(contadorListenerFrag2?.getValorActual().toString())
    super.onResume()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragmento2, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            Fragmento2().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}